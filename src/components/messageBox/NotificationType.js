export default {
    INFO: 'info',
    WARNING: 'warning',
    AUTO_HIDE_INFO: 'auto_hide_info',
    AUTO_HIDE_WARNING: 'auto_hide_warning'
};
